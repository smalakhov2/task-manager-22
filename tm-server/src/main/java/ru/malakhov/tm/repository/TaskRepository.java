package ru.malakhov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.entity.Task;
import ru.malakhov.tm.exception.task.TaskNotFoundException;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void add(final @NotNull String userId, final @NotNull Task task) {
        task.setUserId(userId);
        records.add(task);
    }

    @Override
    public void remove(final @NotNull String userId, final @NotNull Task task) {
        if (!userId.equals(task.getUserId())) return;
        records.remove(task);
    }

    @Override
    public @NotNull List<Task> findAll(final @NotNull String userId) {
        final @NotNull List<Task> result = new ArrayList<>();
        for (final @NotNull Task task: records){
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void clear(final @NotNull String userId) {
        final @NotNull List<Task> tasks = findAll(userId);
        this.records.removeAll(tasks);
    }

    @Override
    public @NotNull Task findOneById(final @NotNull String userId, final @NotNull String id) {
        for (final @NotNull Task task : records) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public @NotNull Task findOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        if (!userId.equals(records.get(index).getUserId())) throw new TaskNotFoundException();
        return records.get(index);
    }

    @Override
    public @NotNull Task findOneByName(final @NotNull String userId, final @NotNull String name) {
        for (final @NotNull Task task : records) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        throw new TaskNotFoundException();
    }

    @Override
    public @NotNull Task removeOneById(final @NotNull String userId, final @NotNull String id) {
        final @NotNull Task task = findOneById(userId, id);
        records.remove(task);
        return task;
    }

    @Override
    public @NotNull Task removeOneByIndex(final @NotNull String userId, final @NotNull Integer index) {
        final @NotNull Task task = findOneByIndex(userId, index);
        remove(userId, task);
        return task;
    }

    @Override
    public @NotNull Task removeOneByName(final @NotNull String userId, final @NotNull String name) {
        final @NotNull Task task = findOneByName(userId, name);
        remove(userId, task);
        return task;
    }

}