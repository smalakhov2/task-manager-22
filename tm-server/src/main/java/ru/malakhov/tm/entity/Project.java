package ru.malakhov.tm.entity;


import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class Project extends AbstractEntity {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    public @NotNull String getUserId() {
        return userId;
    }

    public void setUserId(final @NotNull String userId) {
        this.userId = userId;
    }

    public @NotNull String getName() {
        return name;
    }

    public void setName(final @NotNull String name) {
        this.name = name;
    }

    public @NotNull String getDescription() {
        return description;
    }

    public void setDescription(final @NotNull String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}