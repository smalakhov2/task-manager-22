package ru.malakhov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void clear();

    void merge(@NotNull E e);

    void merge(@NotNull E... e);

    void merge(@NotNull Collection<E> e);

    void load(@NotNull E... e);

    void load(@NotNull Collection<E> e);

    @NotNull
    List<E> getList();

}