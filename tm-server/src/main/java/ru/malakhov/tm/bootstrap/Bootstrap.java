package ru.malakhov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.malakhov.tm.api.endpoint.*;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.repository.ISessionRepository;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.endpoint.*;
import ru.malakhov.tm.enumeration.Role;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.repository.SessionRepository;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.service.*;


import javax.xml.ws.Endpoint;


public final class Bootstrap implements IServiceLocator {

    private @NotNull final IUserRepository userRepository = new UserRepository();

    private @NotNull final IUserService userService = new UserService(userRepository);


    private @NotNull final ITaskRepository taskRepository = new TaskRepository();

    private @NotNull final ITaskService taskService = new TaskService(taskRepository);


    private @NotNull final IPropertyService propertyService = new PropertyService();



    private @NotNull final ISessionRepository sessionRepository = new SessionRepository();

    private @NotNull final ISessionService sessionService = new SessionService(sessionRepository, this);


    private @NotNull final IProjectRepository projectRepository = new ProjectRepository();

    private @NotNull final IProjectService projectService = new ProjectService(projectRepository);

    private @NotNull final IDomainService domainService = new DomainService(taskService, projectService, userService);

    private @NotNull final IDataService dataService = new DataService(domainService);


    private @NotNull final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);

    private @NotNull final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    private @NotNull final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    private @NotNull final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    private @NotNull final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    private @NotNull final IUserEndpoint userEndpoint = new UserEndpoint(this);


    private void registry(final @NotNull Object endpoint){
        final @NotNull String host = propertyService.getServerHost();
        final @NotNull Integer port = propertyService.getServerPort();
        final @NotNull String name = endpoint.getClass().getSimpleName();
        final @NotNull String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        registry(adminEndpoint);
        registry(dataEndpoint);
        registry(projectEndpoint);
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void initProperty() throws Exception {
        propertyService.init();
    }

    private void initUsers(){
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin","admin", Role.ADMIN);
    }

    private static void displayHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    public void run(final String[] args) throws Exception {
        initProperty();
        displayHello();
        initUsers();
        initEndpoint();
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }

    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IDomainService getDomainService() {
        return domainService;
    }

    @Override
    public @NotNull IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public @NotNull IDataService getDataService() {
        return dataService;
    }

    @Override
    public @NotNull ISessionService getSessionService() {
        return sessionService;
    }

}