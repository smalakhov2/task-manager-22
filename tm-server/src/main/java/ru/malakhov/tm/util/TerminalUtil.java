package ru.malakhov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    static @Nullable String nextLine() {
        return SCANNER.nextLine();
    }

    static @NotNull Integer nextNumber() {
        final @Nullable String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            throw new IncorrectIndexException(value);
        }
    }

}