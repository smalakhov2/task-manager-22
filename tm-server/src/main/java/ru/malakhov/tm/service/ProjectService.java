package ru.malakhov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.malakhov.tm.exception.empty.EmptyDescriptionException;
import ru.malakhov.tm.exception.empty.EmptyIdException;
import ru.malakhov.tm.exception.empty.EmptyNameException;
import ru.malakhov.tm.exception.empty.EmptyUserIdException;
import ru.malakhov.tm.exception.system.IncorrectIndexException;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.service.IProjectService;
import ru.malakhov.tm.entity.Project;

import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(final @NotNull IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }


    @Override
    public void create(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
    }

    @Override
    public void create(final @Nullable String userId, final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
    }

    @Override
    public void add(final @Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.add(userId, project);
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable Project project) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (project == null) return;
        projectRepository.remove(userId, project);
    }

    @Override
    public @NotNull List<Project> findAll(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return projectRepository.findAll(userId);
    }

    @Override
    public void clear(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        projectRepository.clear(userId);
    }

    @Override
    public @NotNull Project findOneById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.findOneById(userId, id);
    }

    @Override
    public @NotNull Project findOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @Override
    public @NotNull Project findOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    public @NotNull Project removeOneById(final @Nullable String userId,final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return projectRepository.removeOneById(userId, id);
    }

    @Override
    public @NotNull Project removeOneByIndex(final @Nullable String userId, final @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Override
    public @NotNull Project removeOneByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return projectRepository.removeOneByName(userId, name);
    }

    @Override
    public void updateProjectById(final @Nullable String userId, final @Nullable String id,
                                  final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = findOneById(userId, id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void updateProjectByIndex(final @Nullable String userId, final @Nullable Integer index,
                                     final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @NotNull Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
    }

}