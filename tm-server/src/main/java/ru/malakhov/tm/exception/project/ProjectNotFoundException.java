package ru.malakhov.tm.exception.project;

import ru.malakhov.tm.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found...");
    }

}