package ru.malakhov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tm.malakhov.ru/", name = "TaskEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface TaskEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByNameRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByNameResponse")
    @RequestWrapper(localName = "findOneTaskByName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskByName")
    @ResponseWrapper(localName = "findOneTaskByNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task findOneTaskByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByNameRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByNameResponse")
    @RequestWrapper(localName = "removeOneTaskByName", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskByName")
    @ResponseWrapper(localName = "removeOneTaskByNameResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task removeOneTaskByName(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/updateTaskByIndexRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/updateTaskByIndexResponse")
    @RequestWrapper(localName = "updateTaskByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateTaskByIndex")
    @ResponseWrapper(localName = "updateTaskByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateTaskByIndexResponse")
    public void updateTaskByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByIndexRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByIndexResponse")
    @RequestWrapper(localName = "findOneTaskByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskByIndex")
    @ResponseWrapper(localName = "findOneTaskByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task findOneTaskByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByIdRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findOneTaskByIdResponse")
    @RequestWrapper(localName = "findOneTaskById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskById")
    @ResponseWrapper(localName = "findOneTaskByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindOneTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task findOneTaskById(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/updateTaskByIdRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/updateTaskByIdResponse")
    @RequestWrapper(localName = "updateTaskById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateTaskById")
    @ResponseWrapper(localName = "updateTaskByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.UpdateTaskByIdResponse")
    public void updateTaskById(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByIndexRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByIndexResponse")
    @RequestWrapper(localName = "removeOneTaskByIndex", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskByIndex")
    @ResponseWrapper(localName = "removeOneTaskByIndexResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task removeOneTaskByIndex(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "index", targetNamespace = "")
        java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/clearTaskRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/clearTaskResponse")
    @RequestWrapper(localName = "clearTask", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ClearTask")
    @ResponseWrapper(localName = "clearTaskResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.ClearTaskResponse")
    public void clearTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/createTaskRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/createTaskResponse")
    @RequestWrapper(localName = "createTask", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CreateTask")
    @ResponseWrapper(localName = "createTaskResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.CreateTaskResponse")
    public void createTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "name", targetNamespace = "")
        java.lang.String name,
        @WebParam(name = "description", targetNamespace = "")
        java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByIdRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/removeOneTaskByIdResponse")
    @RequestWrapper(localName = "removeOneTaskById", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskById")
    @ResponseWrapper(localName = "removeOneTaskByIdResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.RemoveOneTaskByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.malakhov.tm.endpoint.Task removeOneTaskById(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session,
        @WebParam(name = "id", targetNamespace = "")
        java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findAllTaskRequest", output = "http://endpoint.tm.malakhov.ru/TaskEndpoint/findAllTaskResponse")
    @RequestWrapper(localName = "findAllTask", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindAllTask")
    @ResponseWrapper(localName = "findAllTaskResponse", targetNamespace = "http://endpoint.tm.malakhov.ru/", className = "ru.malakhov.tm.endpoint.FindAllTaskResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.malakhov.tm.endpoint.Task> findAllTask(
        @WebParam(name = "session", targetNamespace = "")
        ru.malakhov.tm.endpoint.Session session
    );

}