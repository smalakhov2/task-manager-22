package ru.malakhov.tm.endpoint;

import javax.xml.ws.WebFault;


@WebFault(name = "Exception", targetNamespace = "http://endpoint.tm.malakhov.ru/")
public class Exception_Exception extends java.lang.Exception {

    private ru.malakhov.tm.endpoint.Exception exception;

    public Exception_Exception() {
        super();
    }

    public Exception_Exception(String message) {
        super(message);
    }

    public Exception_Exception(String message, java.lang.Throwable cause) {
        super(message, cause);
    }

    public Exception_Exception(String message, ru.malakhov.tm.endpoint.Exception exception) {
        super(message);
        this.exception = exception;
    }

    public Exception_Exception(String message, ru.malakhov.tm.endpoint.Exception exception, java.lang.Throwable cause) {
        super(message, cause);
        this.exception = exception;
    }

    public ru.malakhov.tm.endpoint.Exception getFaultInfo() {
        return this.exception;
    }

}