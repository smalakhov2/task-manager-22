package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "closeResponse", propOrder = {
    "_return"
})
public class CloseResponse {

    @XmlElement(name = "return")
    protected Result _return;

    public Result getReturn() {
        return _return;
    }

    public void setReturn(Result value) {
        this._return = value;
    }

}