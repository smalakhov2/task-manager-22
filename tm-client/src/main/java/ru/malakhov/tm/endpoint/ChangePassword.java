package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changePassword", propOrder = {
    "session",
    "oldPassword",
    "newPassword"
})
public class ChangePassword {

    protected Session session;
    protected String oldPassword;
    protected String newPassword;

    public Session getSession() {
        return session;
    }

    public void setSession(Session value) {
        this.session = value;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String value) {
        this.oldPassword = value;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String value) {
        this.newPassword = value;
    }

}