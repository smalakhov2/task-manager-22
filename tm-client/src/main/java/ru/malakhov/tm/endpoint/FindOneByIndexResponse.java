package ru.malakhov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "findOneByIndexResponse", propOrder = {
    "_return"
})
public class FindOneByIndexResponse {

    @XmlElement(name = "return")
    protected Project _return;

    public Project getReturn() {
        return _return;
    }

    public void setReturn(Project value) {
        this._return = value;
    }

}